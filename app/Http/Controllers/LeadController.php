<?php

namespace App\Http\Controllers;

use App\Mail\SendMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Mail;

class LeadController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     */
    public function store(Request $request)
    {
        Mail::to(env('MAIL_TO'))->send(new SendMail($request->phone, $request->tag));
        //return response()->json($request);
    }

    public function bitrix24(Request $request)
    {
        $data = [
            'fields'=>[
                'SOURCE_ID'=>'SELF',
                'TITLE'=>'МАССАЖ',
                'SOURCE_DESCRIPTION'=>'massage990.vskidke.ru',
                'UTM_SOURCE'=>'trendPro',
                "PHONE"=> [["VALUE"=>$request->phone, "VALUE_TYPE"=> "WORK"]]
            ]
        ];
        $response = Http::post('https://thaispa.bitrix24.ru/rest/1/7p5aj3qhtiby1wty/crm.lead.add',$data);
        return response()->json($response->json());
    }
}
