<?php

namespace App\Http\Livewire;

use App\Mail\SendMail;
use Illuminate\Support\Facades\Mail;
use Livewire\Component;

class FormWidget extends Component
{
    //0 - form, 1-modal
    public $modal;
    public $validated;
    public $phoneNumber = '';
    public $phoneFormat = '/^\+([0-9]{1})\([0-9]{3}\)[0-9]{3}[\s\-][0-9]{2}[\s\-][0-9]{2}$/';
    public $submitDisabled = 1;

    public function mount($modal = 0)
    {
        //0-closed, 1-success, 2-form
        $this->setModal($modal);
    }
    public function render()
    {
        return view('livewire.form-widget');
    }

    public function setModal($modal)
    {
        $this->phoneNumber = '';
        if ($this->modal != $modal) {
            $this->modal = $modal;
        } else {
            $this->modal = 0;
        }
    }

    public function validatePhone()
    {
        $this->validated = preg_match($this->phoneFormat, $this->phoneNumber, $matches);
        if ($this->validated) return true;
        return;
    }

    public function submitPhone()
    {
        if ($this->validatePhone()) {
            //send email
            Mail::to(env('MAIL_TO'))->send(new SendMail($this->phoneNumber));
            $this->phoneNumber = '';
            //goto success modal
            $this->setModal(1);
        }
    }
}
