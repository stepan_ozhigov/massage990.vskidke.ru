module.exports = {
    purge: ["resources/js/*.vue", "resources/js/components/*.vue"],
    theme: {
        screens: {
            sm: "360px",
            md: "768px",
            lg: "1440px",
            xl: "1920px"
        },
        extend: {
            fontFamily: {
                jost: ["Jost"],
                book: ["Book"]
            },
            fontSize: {
                //header
                h1sm: [
                    "2rem",
                    {
                        letterSpacing: "-0.025rem",
                        lineHeight: "2.5rem"
                    }
                ],
                h1md: [
                    "4rem",
                    {
                        letterSpacing: "-0.075rem",
                        lineHeight: "4.5rem"
                    }
                ],
                h1lg: [
                    "4rem",
                    {
                        letterSpacing: "-0.0625rem",
                        lineHeight: "4.5rem"
                    }
                ],
                //header

                //text 1
                psm: [
                    "1rem",
                    {
                        letterSpacing: "0",
                        lineHeight: "1.5rem"
                    }
                ],
                pmd: [
                    "1.5rem",
                    {
                        letterSpacing: "-0.025em",
                        lineHeight: "2rem"
                    }
                ],
                //text 2
                p1sm: [
                    "1.5rem",
                    {
                        letterSpacing: "0",
                        lineHeight: "1.5rem"
                    }
                ],
                p1md: [
                    "2rem",
                    {
                        letterSpacing: "-0.025em",
                        lineHeight: "2rem"
                    }
                ],
                //form
                form: ["1.125rem", {}],

                //footer
                footer: ["0.75rem", {}],
                //footer

                //thanks
                smthanks: [
                    "0.75rem",
                    {
                        letterSpacing: "8px"
                    }
                ],
                mdthanks: [
                    "1rem",
                    {
                        letterSpacing: "8px"
                    }
                ],
                //thanks
                //accepted
                smaccepted: [
                    "2rem",
                    {
                        letterSpacing: "-0.4px",
                        lineHeight: "2.5rem"
                    }
                ],
                mdaccepted: [
                    "2rem",
                    {
                        letterSpacing: "-0.8px",
                        lineHeight: "3.5rem"
                    }
                ],
                lgaccepted: [
                    "2rem",
                    {
                        letterSpacing: "-0.8px",
                        lineHeight: "3.5rem"
                    }
                ],
                //accepted
                //close
                closemodal: ["1.125rem", {}]
                //close
            },
            borderWidth: {
                3: "3px"
            },
            textColor: {
                colorh1: "#f3f2f2",
                phone: "#9b74b2",
                call: "#9b74b2",
                input: "#b3adae",
                footer: "#b3adae",
                close: "#d9d7d7"
            },
            backgroundColor: {
                button: "#451d59"
            },
            borderColor: {
                call: "#9b74b2",
                phoneinput: "#d9d7d7"
            },
            spacing: {
                "-1": "-1rem",
                "-0/45": "-0.45rem",
                "0/15": "0.15rem",
                "9": "2.25rem",
                "14": "3.5rem",
                "18/5": "4.625rem",
                "30": "7.5rem",
                "47": "11.75rem",
                "50": "12.5rem",
                "92": "23rem",
                "106": "26.5rem"
            },
            height: {
                input: "3.5rem",
                mobscreen: "calc(var(--vh)*100)"
            },
            maxWidth: {
                "4rem": "4rem",
                "20/5rem": "20.5rem",
                "23/75rem": "23.75rem",
                "28rem": "28rem",
                "64/5rem": "64.5rem"
            },
            borderRadius: {
                input: "1.75rem"
            },
            inset: {
                "1": "1rem"
            },
            letterSpacing: {
                tight: "-0.25em"
            }
        }
    },
    variants: {
        maxWidth: ["responsive"]
    },
    plugins: [],
    corePlugins: {
        fontStyle: true
    }
};
